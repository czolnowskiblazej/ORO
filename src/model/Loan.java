/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Hyperbook
 */
@Entity(name = "loans")
public class Loan implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "loanId")
    private int loanId;
    @Column(name = "brand")
    private String brand;
    @Column(name = "model")
    private String model;
    @Column(name = "registration")
    private String registration;
    @Column(name = "loanStart")
    private String loanStart;
    @Column(name = "loanEnd")
    private String loanEnd;
    @Column(name = "paydone")
    private String paydone;

    @ManyToOne(cascade = CascadeType.ALL)
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPaydone() {
        return paydone;
    }

    public void setPaydone(String paydone) {
        this.paydone = paydone;
    }

    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getLoanStart() {
        return loanStart;
    }

    public void setLoanStart(String loanStart) {
        this.loanStart = loanStart;
    }

    public String getLoanEnd() {
        return loanEnd;
    }

    public void setLoanEnd(String loanEnd) {
        this.loanEnd = loanEnd;
    }

}
