/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methods;

import java.util.Date;
import model.Administrator;
import model.User;
import java.util.List;
import model.Loan;
import newsessionfactory.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Hyperbook
 */
public class OperationsAdmin implements AdminInterface {

    public static void saveUser(String firstName, String secondName, String city, String street, String phoneNumber, String email, String password) {

        User user = new User();
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setCity(city);
        user.setStreet(street);
        user.setPhoneNumber(phoneNumber);
        user.setEmail(email);
        user.setPassword(password);

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        try {
            session.save(user);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void updateUser(String userId, String firstName, String secondName, String city,
            String street, String phoneNumber, String email, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from users where id='" + userId + "'");
            List<User> usersList = query.list();
            if (usersList.size() > 0) {
                User user = (User) session.load(User.class, Integer.parseInt(userId));
                user.setFirstName(firstName);
                user.setSecondName(secondName);
                user.setCity(city);
                user.setStreet(street);
                user.setPhoneNumber(phoneNumber);
                user.setEmail(email);
                user.setPassword(password);

                session.update(user);
            } else {
                System.out.println("no such user !");
            }

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static void deleteUser(int userId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("from users where id='" + userId + "'");
            List<User> usersList = query.list();

            if (usersList.size() > 0) {
                User user = (User) session.load(User.class, userId);
                session.delete(user);
                System.out.println("user " + userId + " deleted");
            } else {
                System.out.println("no such user!");
            }

            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void showUsers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from users");
            List<User> usersList = query.list();
            System.out.println("id\tfirst name\tsecond name\t\tcity\t\tstreet\t\tphone number\t\temail");
            for (User users : usersList) {

                System.out.print(users.getId() + "\t"
                        + users.getFirstName() + "\t\t"
                        + users.getSecondName() + "\t\t\t"
                        + users.getCity() + "\t\t"
                        + users.getStreet() + "\t\t"
                        + users.getPhoneNumber() + "\t\t"
                        + users.getEmail() + "\n");

            }

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();

        }

    }

    public static boolean loginAdmin(String adminName, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Query queryAdmins = session.createQuery("from administration");
            List<Administrator> admins = queryAdmins.list();

            for (Administrator administration : admins) {

                if (administration.getAdminName().equals(adminName)) {
                    if (administration.getPassword().equals(password)) {
                        return true;
                    }

                }
            }

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public static void addLoanToUser(Integer userId, String brand, String model, String registration, String loanStart, String loanEnd) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {

            transaction = session.beginTransaction();
            Query query = session.createQuery("from users where id='" + userId + "'");
            List<User> usersList = query.list();

            if (usersList.size() > 0) {
                User user = (User) session.load(User.class, userId);

                List<Loan> loanList = user.getLoans();
                Loan loan = new Loan();

                loan.setBrand(brand);
                loan.setModel(model);
                loan.setRegistration(registration);
                loan.setLoanStart(loanStart);
                loan.setLoanEnd(loanEnd);
                loan.setPaydone("no");
                loanList.add(loan);
                loan.setUser(user);
                user.setLoans(loanList);

                session.saveOrUpdate(user);
                session.save(loan);
            } else {
                System.out.println("no such user!");
            }

            session.getTransaction().commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void stopLoan(String registration) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query queryDeleteLoan = session.createQuery("from loans where registration='" + registration + "' ");
            List<Loan> loanToDelete = queryDeleteLoan.list();
            if (loanToDelete.size() > 0) {
                loanToDelete.get(0).setLoanEnd(new Date().toString());
            } else {
                System.out.println("there is no such loan!");
            }

            session.getTransaction().commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void payLoan(String registration) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query queryDeleteLoan = session.createQuery("from loans where registration='" + registration + "' ");
            List<Loan> loanToDelete = queryDeleteLoan.list();

            if (loanToDelete.size() > 0) {
                loanToDelete.get(0).setPaydone("yes " + new Date().toString());
            } else {
                System.out.println("there is no such loan!");
            }

            session.getTransaction().commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void showAllLoans() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Query showLoans = session.createQuery("from loans");
            List<Loan> loansList = showLoans.list();
            System.out.println("loanid\tuserid\tbrand\tmodel\tregistration\t\t\tloanStart\t\t\tloanEnd\t\t\t\tpaydone");
            for (Loan loan : loansList) {

                System.out.println(loan.getLoanId() + "\t"
                        + loan.getUser().getId() + "\t" + loan.getBrand()
                        + "\t" + loan.getModel() + "\t" + loan.getRegistration() + "\t \t \t"
                        + loan.getLoanStart() + "\t\t" + loan.getLoanEnd() + "\t"
                        + loan.getPaydone());

            }

            session.getTransaction().commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void showUserLoans(String userId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("from users where id='" + userId + "'");
            List<User> usersList = query.list();

            if (usersList.size() > 0) {
                User user = (User) session.load(User.class, Integer.parseInt(userId));
                List<Loan> userLoans = user.getLoans();

                System.out.println("loanid \t userid \t \t brand \t \t model \t registration \t \t loanStart \t \t \t loanEnd \t \t \t \t paydone");

                for (int i = 0; i < userLoans.size(); i++) {

                    System.out.println(userLoans.get(i).getLoanId() + " \t " + userLoans.get(i).getUser().getId() + "\t \t \t" + userLoans.get(i).getBrand()
                            + "\t \t" + userLoans.get(i).getModel() + "\t" + userLoans.get(i).getRegistration() + "\t \t"
                            + userLoans.get(i).getLoanStart() + "\t" + userLoans.get(i).getLoanEnd() + "\t \t" + userLoans.get(i).getPaydone());
                }
            } else {
                System.out.println("no such user!");
            }

            session.getTransaction().commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void showActionsAdmin() {
        System.out.println("\n\n-----------------------------\n"
                + "0-delete user\n"
                + "1-add user\n"
                + "2-edit user\n"
                + "3-show users\n"
                + "4-add loan\n"
                + "5-show one user loans\n"
                + "6-show all loans\n"
                + "7-get payment\n"
                + "8-end loan\n"
                + "9-end program\n"
                + "-----------------------------\n");

    }

}
