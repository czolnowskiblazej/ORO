/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methods;

import model.User;
import java.util.List;
import model.Loan;
import newsessionfactory.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Hyperbook
 */
public class OperationsUser implements UserInterface {

    public static boolean loginUser(String userEmail, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("from users");
            List<User> usersList = query.list();

            for (User users : usersList) {

                if (users.getEmail().equals(userEmail)) {
                    if (users.getPassword().equals(password)) {
                        return true;
                    }

                }
            }

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public static void showUser(String userEmail) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("from users");
            List<User> usersList = query.list();

            for (User users : usersList) {

                if (users.getEmail().equals(userEmail)) {
                    System.out.println("id\tfirst name\tsecond name\tcity\tstreet\tphone number    email");
                    System.out.println(users.getId() + "\t" + users.getFirstName() + "\t\t" + users.getSecondName()
                            + "\t\t" + users.getCity() + "\t" + users.getStreet() + "\t" + users.getPhoneNumber() + "\t \t" + users.getEmail());

                }
            }

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void editMyself(String firstName, String secondName, String city, String street, String phoneNumber, String email, String oldEmailAddress) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("from users where email like '" + oldEmailAddress + "'");
            List<User> CurrentUser = query.list();
            if (!"".equals(firstName)) {
                CurrentUser.get(0).setFirstName(firstName);
            }

            if (!"".equals(secondName)) {
                CurrentUser.get(0).setSecondName(secondName);
            }

            if (!"".equals(city)) {
                CurrentUser.get(0).setCity(city);
            }

            if (!"".equals(street)) {
                CurrentUser.get(0).setStreet(street);
            }

            if (!"".equals(phoneNumber)) {
                CurrentUser.get(0).setPhoneNumber(phoneNumber);
            }

            if (!"".equals(email)) {
                CurrentUser.get(0).setEmail(email);
            }

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public static void showMyLoan(String eMailAdress) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from users where email like '" + eMailAdress + "'");
            List<User> userDetails = query.list();

            Integer CurrentUserIdInt = userDetails.get(0).getId();
            User user = (User) session.load(User.class, CurrentUserIdInt);
            List<Loan> userLoans = user.getLoans();
            System.out.println("loanid\tuserid\tbrand\tmodel\tregistration\tloanStart\t\t\tloanEnd\t\t\t\tpaydone");
            for (int i = 0; i < userLoans.size(); i++) {
                System.out.println(userLoans.get(i).getLoanId() + "\t" + userLoans.get(i).getUser().getId() + "\t" + userLoans.get(i).getBrand()
                        + "\t" + userLoans.get(i).getModel() + "\t" + userLoans.get(i).getRegistration() + "\t"
                        + userLoans.get(i).getLoanStart() + "\t" + userLoans.get(i).getLoanEnd() + "\t" + userLoans.get(i).getPaydone());
            }

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static void showActionsUser() {
        System.out.println("\n\n\n------------------------------------");
        System.out.println("1-show personal data");
        System.out.println("2-edit personal data");
        System.out.println("3-show your loans");
        System.out.println("4-end program");
        System.out.println("------------------------------------\n\n\n");
    }
}
