/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methods;

import java.util.Scanner;

/**
 *
 * @author Hyperbook
 */
public class DataReader {

    public static int readInt(String message) {

        System.out.println(message);
        Scanner readVariable = new Scanner(System.in);
        while (!readVariable.hasNextInt()) {

            System.out.println("That's not a number!");
            readVariable.next();
        }
        int numberToReturn = readVariable.nextInt();

        return numberToReturn;
    }

    public static String readString(String message) {

        System.out.println(message);
        Scanner readVariable = new Scanner(System.in);
        String variableToReturn = readVariable.nextLine();

        return variableToReturn;
    }

    public static void Wait() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Press any key to continue . . .");
        scan.nextLine();
    }

}
