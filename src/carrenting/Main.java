/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrenting;

import methods.DataReader;
import static methods.DataReader.readInt;
import static methods.DataReader.readString;
import methods.OperationsAdmin;
import methods.OperationsUser;

/**
 *
 * @author Hyperbook
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String emailUser;
        String passwordUser;
        String adminName;
        String adminPassword;
        int userId;

        while (true) {

            int adminOrUserLogin = readInt("1-login as user\n2-login as admin");

            if (adminOrUserLogin == 1) {

                emailUser = "";
                passwordUser = "";
                emailUser = readString("enter your email:");
                passwordUser = readString("enter your password:");

                if (OperationsUser.loginUser(emailUser, passwordUser) != true) {
                    System.out.println("unauthorized access ! bye");
                    DataReader.Wait();
                    System.exit(0);
                }

                System.out.println("login succesful . Welcome user");

                while (true) {
                    OperationsUser.showActionsUser();
                    int userAction = readInt("What do you want to do?\n------------------------------------");
                    switch (userAction) {
                        case 1:
                            OperationsUser.showUser(emailUser);
                            DataReader.Wait();
                            break;
                        case 2:
                            System.out.println("current personal info:");
                            OperationsUser.showUser(emailUser);

                            String newFirstName = "";
                            String newSecondName = "";
                            String newCity = "";
                            String newStreet = "";
                            String newPhoneNumber = "";
                            String newEmail = "";

                            newFirstName = readString("please write your first name (click enter to skip this field)");
                            newSecondName = readString("please write your second name (click enter to skip this field)");
                            newCity = readString("please write your city (click enter to skip this field)");
                            newStreet = readString("please write your street (click enter to skip this field)");
                            newPhoneNumber = readString("please write your phone no (click enter to skip this field)");
                            newEmail = readString("please write your email (click enter to skip this field)");

                            OperationsUser.editMyself(newFirstName, newSecondName, newCity, newStreet, newPhoneNumber, newEmail, emailUser);
                            System.out.println("changed personal info");
                            DataReader.Wait();
                            break;

                        case 3:
                            OperationsUser.showMyLoan(emailUser);
                            DataReader.Wait();
                            break;
                        case 4:
                            System.exit(0);
                            break;
                        default:
                            System.out.println("No action detected!");
                            DataReader.Wait();
                            break;

                    }

                }

            }

            if (adminOrUserLogin == 2) {

                adminName = "";
                adminPassword = "";
                adminName = readString("enter your admin login:");
                adminPassword = readString("enter your password:");

                if (OperationsAdmin.loginAdmin(adminName, adminPassword) != true) {
                    System.out.println("unauthorized access ! bye");
                    System.exit(0);
                }

                System.out.println("login succesful . Welcome admin");

                while (true) {
                    OperationsAdmin.showActionsAdmin();
                    int adminAction = readInt("What do you want to do?\n------------------------------------");
                    switch (adminAction) {
                        case 0:

                            OperationsAdmin.showUsers();
                            DataReader.Wait();
                            userId = readInt("Which user you want to delete ?");

                            OperationsAdmin.deleteUser(userId);

                            DataReader.Wait();
                            break;

                        case 1:

                            String newFirstName = "";
                            String newSecondName = "";
                            String newCity = "";
                            String newStreet = "";
                            String newPhoneNumber = "";
                            String newEmail = "";
                            String newPassword = "";

                            newFirstName = readString("please write first name ");
                            newSecondName = readString("please write second name ");
                            newCity = readString("please write city");
                            newStreet = readString("please write street ");
                            newPhoneNumber = readString("please write  phone no");
                            newEmail = readString("please write email");
                            newPassword = readString("please write new user password");

                            OperationsAdmin.saveUser(newFirstName, newSecondName, newCity, newStreet, newPhoneNumber, newEmail, newPassword);
                            DataReader.Wait();
                            break;
                        case 2:
                            OperationsAdmin.showUsers();
                            DataReader.Wait();

                            newFirstName = "";
                            newSecondName = "";
                            newCity = "";
                            newStreet = "";
                            newPhoneNumber = "";
                            newEmail = "";
                            newPassword = "";

                            userId = readInt("please write which user do you edit?");
                            newFirstName = readString("please write first name ");
                            newSecondName = readString("please write second name ");
                            newCity = readString("please write city");
                            newStreet = readString("please write street ");
                            newPhoneNumber = readString("please write  phone no");
                            newEmail = readString("please write email");
                            newPassword = readString("please write new user password");

                            OperationsAdmin.updateUser(Integer.toString(userId), newFirstName, newSecondName, newCity, newStreet, newPhoneNumber, newEmail, newPassword);
                            DataReader.Wait();
                            break;

                        case 3:

                            OperationsAdmin.showUsers();
                            DataReader.Wait();
                            break;
                        case 4:
                            OperationsAdmin.showUsers();
                            DataReader.Wait();

                            String carBrand = "";
                            String carModel = "";
                            String carRegistration = "";
                            String loanStart = "";
                            String loanEnd = "";

                            userId = DataReader.readInt("Which user is renting?");
                            carBrand = DataReader.readString("Insert car brand");
                            carModel = DataReader.readString("Insert car model");
                            carRegistration = DataReader.readString("Insert car registration");
                            loanStart = DataReader.readString("Insert renting start");
                            loanEnd = DataReader.readString("Insert renting stop");

                            OperationsAdmin.addLoanToUser(userId, carBrand, carModel, carRegistration, loanStart, loanEnd);
                            DataReader.Wait();

                            break;

                        case 5:
                            OperationsAdmin.showUsers();
                            DataReader.Wait();

                            userId = DataReader.readInt("Which user loans do you want to see?");

                            OperationsAdmin.showUserLoans(Integer.toString(userId));
                            DataReader.Wait();
                            break;

                        case 6:
                            OperationsAdmin.showAllLoans();
                            DataReader.Wait();
                            break;

                        case 7:
                            carRegistration = "";
                            OperationsAdmin.showAllLoans();
                            DataReader.Wait();
                            carRegistration = DataReader.readString("Which car is beeing payed? give registration");
                            OperationsAdmin.payLoan(carRegistration);
                            DataReader.Wait();

                            break;

                        case 8:
                            carRegistration = "";
                            OperationsAdmin.showAllLoans();
                            DataReader.Wait();
                            carRegistration = DataReader.readString("Which car is beeing stoped loaning? Give registration");
                            OperationsAdmin.stopLoan(carRegistration);
                            DataReader.Wait();
                            break;

                        case 9:
                            System.exit(0);
                            break;

                        default:
                            System.out.println("No action detected!");
                            DataReader.Wait();
                            break;

                    }

                }

            }

            System.out.println("no action detected !\n");
        }

    }

}
